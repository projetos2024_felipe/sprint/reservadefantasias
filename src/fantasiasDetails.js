import React, { useState } from "react"; // Importa o React e a função useState do pacote 'react'
import { View, TouchableOpacity, StyleSheet, FlatList, ScrollView, Image, Text, Dimensions, Button } from "react-native"; // Importa componentes essenciais do React Native para construir a interface
import { useNavigation } from "@react-navigation/native"; // Importa o hook useNavigation do pacote '@react-navigation/native' para acessar a navegação
import { TextInput } from "react-native-gesture-handler"; // Importa o componente TextInput do pacote 'react-native-gesture-handler' para entrada de texto

function Detalhes({ route }) { // Define o componente Detalhes e recebe os parâmetros de rota
    const { fantasias } = route.params; // Extrai os dados das fantasias dos parâmetros de rota
    const navigation = useNavigation(); // Inicializa a navegação usando o hook useNavigation

    return ( // Retorna a estrutura da interface do componente
        <View style={styles.container}> 
            <View style={styles.header}> 
                <Button title="Voltar" onPress={() => navigation.goBack()} /> 
            </View>
            <Image style={styles.image} source={fantasias.image} /> 
            <View style={styles.textContainer}> 
                <Text style={styles.text}>Nome: {fantasias.name}</Text> 
                <Text style={styles.text}>Preço: {fantasias.preço}</Text> 
            </View>
            <TouchableOpacity style={styles.button}> 
                <Text style={styles.buttonText}>Reservar</Text> 
            </TouchableOpacity>
        </View> // Fecha o contêiner principal do componente
    ); // Fecha o retorno da função
} // Fecha a definição do componente Detalhes

const styles = StyleSheet.create({ // Define os estilos usando StyleSheet
    container: { // Estilos para o contêiner principal
        flex: 1, // Ocupa todo o espaço disponível
        alignItems: 'center', // Alinha os itens no centro horizontalmente
        paddingBottom: 100, // Adiciona espaço na parte inferior para acomodar o botão "Reservar"
    },
    header: { // Estilos para o cabeçalho
        position: 'absolute', // Define a posição do cabeçalho como absoluta
        top: 60, // Define a distância do topo
        left: 20, // Define a distância da esquerda
    },
    button: { // Estilos para o botão
        backgroundColor: "#7435F0", // Cor de fundo do botão
        paddingVertical: 15, // Preenchimento vertical
        paddingHorizontal: 30, // Preenchimento horizontal
        borderRadius: 5, // Raio da borda
        marginBottom: 20, // Margem inferior
        width: "80%", // Largura do botão
        position: 'absolute', // Define a posição do botão como absoluta
        bottom: 20, // Define a distância do final da tela
    },
    buttonText: { // Estilos para o texto do botão
        color: "white", // Cor do texto
        fontSize: 20, // Tamanho da fonte
        textAlign: "center", // Alinhamento do texto
        fontWeight: "bold", // Peso da fonte
    },
    textContainer: { // Estilos para o contêiner do texto
        alignItems: 'center', // Alinha os itens no centro horizontalmente
        marginTop: 100, // Margem superior
    },
    text: { // Estilos para o texto
        marginBottom: 10, // Margem inferior
        top: 105, // Define a distância do topo
        fontSize: 20, // Tamanho da fonte
        fontWeight: "bold", // Peso da fonte
    },
    image: { // Estilos para a imagem
        width: 300, // Largura da imagem
        height: 400, // Altura da imagem
        top: 130, // Define a distância do topo
    },
});

export default Detalhes; // Exporta o componente Detalhes

