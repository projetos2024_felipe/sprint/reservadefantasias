import axios from "axios"; // Importa o pacote axios para lidar com requisições HTTP

const api = axios.create({ // Cria uma instância do axios com configurações personalizadas
    baseURL: "http://10.89.234.164:5000/api/", // Define a URL base para todas as requisições
    headers: { // Define os cabeçalhos padrão para todas as requisições
        'accept': 'application/json' // Define o cabeçalho 'accept' como 'application/json'
    },
});

const sheets = { // Define um objeto chamado sheets
    postCadastro: (user) => api.post("usuarioPost", user), // Define um método chamado postCadastro que faz uma requisição POST para a rota 'usuarioPost' da API
    postLogin: (user) => api.post("login", user), // Define um método chamado postLogin que faz uma requisição POST para a rota 'login' da API
};

export default sheets; // Exporta o objeto sheets para ser utilizado em outros arquivos

